acdx_commerce_products
======================

This module is an enhanced autocomplete widget for the commerce product
reference field. The original idea is from the autocomplete_deluxe module,
but I made some changes to use this widget for commerce products.

USAGE
======================

- Enable module
- Go to the commerce product reference field widget settings
  (e.g. admin/structure/types/manage/{nodetype}/fields/{yourfield}/widget-type)
  and choose Autocomplete Deluxe (commerce products)

AUTHOR/MAINTAINER
======================

- Jeroen Tubex (JeroenT)

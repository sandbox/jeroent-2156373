/**
 * @file:
 * Converts textfield to a autocomplete deluxe widget.
 */

(function ($) {
    Drupal.acdx_commerce_products = Drupal.acdx_commerce_products || {};

    Drupal.behaviors.acdx_commerce_products = {
        attach: function (context) {
            var autocomplete_settings = Drupal.settings.acdx_commerce_products;

            $('input.acdx-commerce-products-form').once(function () {
                new Drupal.acdx_commerce_products.MultipleWidget(this, autocomplete_settings[$(this).attr('id')]);
            });
        }
    };

    /**
     * Autogrow plugin which auto resizes the input of the multiple value.
     *
     * http://stackoverflow.com/questions/931207/is-there-a-jquery-autogrow-plugin-for-text-fields
     *
     */
    $.fn.autoGrowInput = function (o) {

        o = $.extend({
            maxWidth: 1000,
            minWidth: 0,
            comfortZone: 70
        }, o);

        this.filter('input:text').each(function () {

            var minWidth = o.minWidth || $(this).width(),
                val = '',
                input = $(this),
                testSubject = $('<tester/>').css({
                    position: 'absolute',
                    top: -9999,
                    left: -9999,
                    width: 'auto',
                    fontSize: input.css('fontSize'),
                    fontFamily: input.css('fontFamily'),
                    fontWeight: input.css('fontWeight'),
                    letterSpacing: input.css('letterSpacing'),
                    whiteSpace: 'nowrap'
                }),
                check = function () {

                    if (val === (val = input.val())) {
                        return;
                    }

                    // Enter new content into testSubject
                    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g, '&nbsp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    testSubject.html(escaped);

                    // Calculate new width + whether to change
                    var testerWidth = testSubject.width(),
                        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
                        currentWidth = input.width(),
                        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
                            || (newWidth > minWidth && newWidth < o.maxWidth);

                    // Animate width
                    if (isValidWidthChange) {
                        input.width(newWidth);
                    }

                };

            testSubject.insertAfter(input);

            $(this).bind('keyup keydown blur update', check);

        });

        return this;
    };

    Drupal.acdx_commerce_products.empty = {label: '- ' + Drupal.t('None') + ' -', value: "" };

    /**
     * EscapeRegex function from jquery autocomplete, is not included in drupal.
     */
    Drupal.acdx_commerce_products.escapeRegex = function (value) {
        return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/gi, "\\$&");
    };

    /**
     * Filter function from jquery autocomplete, is not included in drupal.
     */
    Drupal.acdx_commerce_products.filter = function (array, product) {
        var matcher = new RegExp(Drupal.acdx_commerce_products.escapeRegex(product), "i");
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };

    Drupal.acdx_commerce_products.Widget = function () {
    };

    Drupal.acdx_commerce_products.Widget.prototype.uri = null;

    Drupal.acdx_commerce_products.Widget.prototype.init = function (settings) {
        if ($.browser.msie && $.browser.version === "6.0") {
            return;
        }

        this.id = settings.input_id;
        this.jqObject = $('#' + this.id);

        this.uri = settings.uri;
        this.cardinality = settings.cardinality;
        this.required = settings.required;
        this.limit = settings.limit;
        this.default_value = settings.default_value;

        this.wrapper = '""';

        if (typeof settings.delimiter == 'undefined') {
            this.delimiter = true;
        } else {
            this.delimiter = settings.delimiter.charCodeAt(0);
        }

        this.items = {};

        var self = this;
        var parent = this.jqObject.parent();
        var parents_parent = this.jqObject.parent().parent();

        parents_parent.append(this.jqObject);
        parent.remove();
        parent = parents_parent;

        var generateValues = function (data, product) {
            var result = new Array();
            for (var products in data) {
                if (self.acceptProduct(products)) {
                    result.push({
                        label: data[products],
                        value: products
                    });
                }
            }
            return result;
        };

        var cache = {}
        var lastXhr = null;

        this.source = function (request, response) {
            var product = request.term;
            if (product in cache) {
                response(generateValues(cache[product], product));
                return;
            }

            // Some server collapse two slashes if the product is empty, so insert at
            // least a whitespace. This whitespace will later on be trimmed in the
            // autocomplete callback.
            if (!product) {
                product = " ";
            }
            var url = settings.uri + '/' + product + '/' + self.limit;
            lastXhr = $.getJSON(url, request, function (data, status, xhr) {
                cache[product] = data;
                if (xhr === lastXhr) {
                    response(generateValues(data, product));
                }
            });
        };

        this.jqObject.autocomplete({
            'source': this.source,
            'minLength': settings.min_length
        });

        var jqObject = this.jqObject;
        var throbber = $('<div class="acdx-commerce-products-throbber acdx-commerce-products-closed">&nbsp;</div>').insertAfter(jqObject);

        this.jqObject.bind("autocompletesearch", function (event, ui) {
            throbber.removeClass('acdx-commerce-products-closed');
            throbber.addClass('acdx-commerce-products-open');
        });

        this.jqObject.bind("autocompleteopen", function (event, ui) {
            throbber.addClass('acdx-commerce-products-closed');
            throbber.removeClass('acdx-commerce-products-open');
        });

        // Monkey patch the _renderItem function jquery so we can highlight the
        // text, that we already entered.
        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
            var t = item.label;
            if (this.term != "") {
                var escapedValue = Drupal.acdx_commerce_products.escapeRegex(this.term);
                var re = new RegExp('()*""' + escapedValue + '""|' + escapedValue + '()*', 'gi');
                var t = item.label.replace(re, "<span class='acdx-commerce-products-highlight-char'>$&</span>");
            }
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + t + "</a>")
                .appendTo(ul);
        };
    };

    Drupal.acdx_commerce_products.Widget.prototype.generateValues = function (data) {
        var result = new Array();
        for (var index in data) {
            result.push(data[index]);
        }
        return result;
    };

    /**
     * Creates a multiple selecting widget.
     */
    Drupal.acdx_commerce_products.MultipleWidget = function (input, settings) {
        this.init(settings);
        this.setup();
    };

    Drupal.acdx_commerce_products.MultipleWidget.prototype = new Drupal.acdx_commerce_products.Widget();
    Drupal.acdx_commerce_products.MultipleWidget.prototype.items = new Object();

    Drupal.acdx_commerce_products.MultipleWidget.prototype.acceptProduct = function (product) {
        // Accept only products, that are not in our items list.
        return !(product in this.items);
    };

    Drupal.acdx_commerce_products.MultipleWidget.Item = function (widget, item) {

        this.value = item.value;
        this.element = $('<span class="acdx-commerce-products-item">' + item.label + '</span>');
        this.widget = widget;
        this.item = item;
        var self = this;

        var close = $('<a class="acdx-commerce-products-item-delete" href="javascript:void(0)"></a>').appendTo(this.element);
        // Use single quotes because of the double quote encoded stuff.
        var input = $('<input type="hidden" value=\'' + this.value + '\'/>').appendTo(this.element);

        close.mousedown(function () {
            self.remove(item);
        });
    };

    Drupal.acdx_commerce_products.MultipleWidget.Item.prototype.remove = function () {
        this.element.remove();
        var values = this.widget.valueForm.val();
        var escapedValue = Drupal.acdx_commerce_products.escapeRegex(this.item.value);
        var regex = new RegExp('()*""' + escapedValue + '""()*', 'gi');
        this.widget.valueForm.val(values.replace(regex, ''));
        delete this.widget.items[this.value];
        var count = 0;
        for (var item in this.widget.items) {
            ++count;
        }
        if (this.widget.cardinality > count || this.widget.cardinality == -1) {
            this.widget.jqObject.show();
        }
    };

    Drupal.acdx_commerce_products.MultipleWidget.prototype.setup = function () {
        var jqObject = this.jqObject;
        var parent = jqObject.parent();
        var value_container = jqObject.parent().parent().children('.acdx-commerce-products-value-container');
        var value_input = value_container.children().children();
        var items = this.items;
        var cardinality = this.cardinality;
        var self = this;
        this.valueForm = value_input;

        // Override the resize function, so that the suggestion list doesn't resizes
        // all the time.
        jqObject.data("autocomplete")._resizeMenu = function () {
        };

        jqObject.show();

        value_container.hide();

        // Add the default values to the box.
        var default_values = this.default_value;
        var empty_default_values = true;
        for (var index in default_values) {
            var value = default_values[index];
            if (value != '') {
                // If a product is encoded in double quotes, then the label should have
                // no double quotes.
                value = '"' + value + '"';
                var label = value.match(/["][\w|\s|\D|]*["]/gi) !== null ? value.substr(1, value.length - 2) : value;
                var item = {
                    label: label,
                    value: index
                };
                var item = new Drupal.acdx_commerce_products.MultipleWidget.Item(self, item);
                item.element.insertBefore(jqObject);
                items[item.value] = item;
                empty_default_values = false;
            }
        }
        var count = 0;
        for (var item in items) {
            ++count;
        }
        if (cardinality <= count && cardinality != -1) {
            jqObject.hide();
        }

        jqObject.addClass('acdx-commerce-products-multiple');
        parent.addClass('acdx-commerce-products-multiple');

        // Adds a value to the list.
        this.addValue = function (ui_item) {
            var item = new Drupal.acdx_commerce_products.MultipleWidget.Item(self, ui_item);
            item.element.insertBefore(jqObject);
            items[ui_item.value] = item;
            var new_value = ' ' + self.wrapper + ui_item.value + self.wrapper;
            var values = value_input.val();
            value_input.val(values + new_value);
            jqObject.val('');
            var count = 0;
            for (var item in items) {
                ++count;
            }
            if (cardinality <= count && cardinality != -1) {
                jqObject.hide();
            }
        };

        parent.mouseup(function () {
            var count = 0;
            for (var item in items) {
                ++count;
            }
            if (cardinality > count || cardinality == -1) {
                jqObject.autocomplete('search', '');
            }
        jqObject.focus();
    });

    jqObject.bind("autocompleteselect", function (event, ui) {
        self.addValue(ui.item);
        jqObject.width(25);
        // Return false to prevent setting the last product as value for the jqObject.
        return false;
    });

    jqObject.bind("autocompletechange", function (event, ui) {
        jqObject.val('');
    });

    jqObject.blur(function () {
        var last_element = jqObject.parent().children('.acdx-commerce-products-item').last();
        last_element.removeClass('acdx-commerce-products-item-focus');
    });

    var clear = false;

    jqObject.keypress(function (event) {
        var value = jqObject.val();
        // If a comma was entered and there is none or more then one comma,or the
        // enter key was entered, then enter the new product.
        if ((event.which == self.delimiter && (value.split('"').length - 1) != 1) || (event.which == 13 && jqObject.val() != "")) {
            value = value.substr(0, value.length);
            if (typeof self.items[value] == 'undefined' && value != '') {
                var ui_item = {
                    label: value,
                    value: value
                };
                self.addValue(ui_item);
            }
            clear = true;
            if (event.which == 13) {
                return false;
            }
        }

        // If the Backspace key was hit and the input is empty
        if (event.which == 8 && value == '') {
            var last_element = jqObject.parent().children('.acdx-commerce-products-item').last();
            // then mark the last item for deletion or deleted it if already marked.
            if (last_element.hasClass('acdx-commerce-products-item-focus')) {
                var value = last_element.children('input').val();
                self.items[value].remove(self.items[value]);
                jqObject.autocomplete('search', '');
            } else {
                last_element.addClass('acdx-commerce-products-item-focus');
            }
        } else {
            // Remove the focus class if any other key was hit.
            var last_element = jqObject.parent().children('.acdx-commerce-products-item').last();
            last_element.removeClass('acdx-commerce-products-item-focus');
        }
    });

    jqObject.autoGrowInput({
        comfortZone: 50,
        minWidth: 10,
        maxWidth: 460
    });

    jqObject.keyup(function (event) {
        if (clear) {
            // Trigger the search, so it display the values for an empty string.
            jqObject.autocomplete('search', '');
            jqObject.val('');
            clear = false;
            // Return false to prevent entering the last character.
            return false;
        }
    });
};
})
(jQuery);
